#include <ArduinoJson.h>
#define POPschool

#include <DHT.h>
#include "DHT.h"
#define BASEDELAY 60000

#define DHTPIN 4
// créer l'objet "sensor" de type DHT:
#define DHTTYPE DHT11
DHT sensor(DHTPIN, DHTTYPE);
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti WiFiMulti;

//On charge le fichier config.h
#include "config.h";
// #define mySSID"..."
// #define PASSWORD "..."
// #define APIKEY


#include<HTTPClient.h>
HTTPClient http_owm;
HTTPClient http_node;

//LED Function
void REDon() {
  digitalWrite(14, HIGH);
}
void REDoff() {
  digitalWrite(14, LOW);
}
void BLUon() {
  digitalWrite(13, HIGH);
}
void BLUoff() {
  digitalWrite(13, LOW);

}
int Tmin = 999 ;
int Tmax = 999 ;

//creates a doc for Json (notre obj forecast)
//1500 for 1 fragment, octets
StaticJsonDocument<1500> forecast;
StaticJsonDocument<200> actions;


void setup() { // Initialisation + connection Wifi
  //connecting dht11
  sensor.begin();
  Serial.begin(115200);
  delay(10);
  //connecting wifi
  WiFiMulti.addAP(mySSID, PASSWORD );
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  for (int i = 0; i <= 10; i++) {
    digitalWrite(13, HIGH);
    digitalWrite(14, LOW);
    delay(100);
    digitalWrite(14, HIGH);
    digitalWrite(13, LOW);
    delay(100);
  }
}
void send_data() { // sending data to nodered server
  delay(2000);
  float h = sensor.readHumidity();
  float t = sensor.readTemperature();
  if (Tmin == 999) {
    Tmin = t;
    Tmax = t;
  }
  { //debug T et H
    Serial.print("H:");
    Serial.print(h);
    Serial.print(",");
    Serial.print("T:");
    Serial.println(t);
  }
  //host to contact
  String host = "10.130.1.224:1880";
  //URL to get:
  String url = "/meteal/" + String(t) + "/" + String(h);
  http_node.begin("http://" + host + url);

  int retCode = http_node.GET();
  Serial.print("[HTTP]Return code=");
  Serial.println(retCode);
  if (retCode > 0) {
    //all is ok (at least)
    if (retCode == HTTP_CODE_OK) {
      REDoff();
      BLUon();
      String content = http_node.getString();
      Serial.println(content);
    } else {
      BLUoff();
      REDon();
      Serial.println("[HTTP]:Oooooops..... Something went wrong.");
    }
  } else {
    BLUoff();
    REDon();
    //an error occured...
    Serial.print("[HTTP]GET Failed,error:");
    Serial.print(retCode);
  }
  //recupération des T min et max:
  if (t < Tmin) {
    Tmin = t;
  }
  if (t > Tmax) {
    Tmax = t;
  }
  { //debug tmin tmax
    Serial.print("Tmax:");
    Serial.println(Tmax);
    Serial.print("Tmin:");
    Serial.println(Tmin);
  }
  http_node.end();

}
void get_data() {
  String host = "api.openweathermap.org";
  //URL To get
  // q=city,countrocode
  //cnt = count of result
  String url = "/data/2.5/forecast?q=willems,fr&cnt=1&appid=" + String(APIKEY);
  http_owm.begin("http://" + host + url);
  //création de requete
  int retCode = http_owm.GET();
  Serial.print("[HTTP]Return code=");
  Serial.println(retCode);


  if (retCode > 0) {
    //all is ok (at least)
    if (retCode == HTTP_CODE_OK) {
      REDoff();
      BLUon();
      String content = http_owm.getString();
      //we have our data ready
      Serial.println(content);
      //lets try to parse it:

      DeserializationError error = deserializeJson(forecast, content);

      // Test if parsing succeeds.
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;

      } //end if parse error
      //parsing was ok , grabbing values:
      float Wspeed = forecast["list"][0]["wind"]["speed"];
      int Wdir = forecast["list"][0]["wind"]["deg"];

      Serial.print("Wind speed(m/s)=");
      Serial.println(Wspeed);
      Serial.print("Wind direction(degrés)=");
      Serial.println(Wdir);
    }
    else {
      BLUoff();
      REDon();
      //an error occured...
      Serial.print("[HTTP]GET Failed,error:");
      Serial.print(retCode);
    }
  }
}
void get_actions() {//get action from NodeRed
  //host to contact (NodeRed server)
  String host = "10.130.1.224:1880";

  //URL To get
  String url = "/getactions";
  http_owm.begin("http://" + host + url);
  //création de requete
  int retCode = http_owm.GET();
  Serial.print("[HTTP]Return code=");
  Serial.println(retCode);


  if (retCode > 0) {
    //all is ok (at least)
    if (retCode == HTTP_CODE_OK) {
      REDoff();
      BLUon();
      String content = http_owm.getString();
      //we have our data ready
      Serial.println(content);
      //lets try to parse it:

      DeserializationError error = deserializeJson(actions, content);

      // Test if parsing succeeds.
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      } //end if parse error
      //parsing was ok , grabbing values:

      bool reponserecue = false;

      if (actions["porte"] == 1) {
        Serial.println("OUVERTURE PORTE !!!");
        reponserecue= true;
      }
      if (actions["porte"] == 0) {
        Serial.println("FERMETURE PORTE !!! ");
        reponserecue= true;
      }

      if (reponserecue == false )  {
        Serial.println("Je sais pas, j'ai rien fais !!!");
      }
    }
  }
}

void loop() {         // main loop
  //send_data();
  //  get_data();
  get_actions();
  delay(10000);

}
